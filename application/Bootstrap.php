<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initDoctype()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');
    }

    public function _initConfig()
    {
        ini_set('mssql.charset', 'UTF-8');
        Zend_Registry::set('config',
            new Zend_Config($this->getOptions())
        );


    }

    protected function _initDatabase(){
        // get config from config/application.ini
        $config = $this->getOptions();
//        Zend_Debug::dump(APPLICATION_ENV);die;
//Zend_Debug::dump($config);die;
        $db = Zend_Db::factory($config['resources']['db']['adapter'], $config['resources']['db']['params']);

        //set default adapter
        Zend_Db_Table::setDefaultAdapter($db);

        //save Db in registry for later use
        Zend_Registry::set("db", $db);
    }
//    protected function _initLocale()
//    {
//        $locale = new Zend_Locale('ru_RU');
//        Zend_Registry::set('Zend_Locale', $locale);
//
//        $defaultFormTranslator = new Zend_Translate_Adapter_Array( APPLICATION_PATH . '/resources/languages/ru/Zend_Validate.php');
//        Zend_Validate_Abstract::setDefaultTranslator($defaultFormTranslator);
//        Zend_Registry::set("Zend_Translate", $defaultFormTranslator);
//    }
}

