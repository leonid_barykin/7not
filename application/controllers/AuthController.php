<?php

/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 03.03.2016
 * Time: 11:58
 */
class AuthController extends Zend_Controller_Action
{
    function init()
    {
        $this->view->baseUrl = $this->_request->getBaseUrl();
    }

    public function indexAction()
    {
        Zend_Loader::loadClass('DB_Users');
        $db = new DB_Users();

        // action body
    }

    public function registerAction()
    {
//        Zend_Loader::loadClass('DB_Users');
//        $db = new DB_Users();

        // action body
    }

    public function checkDataUsageAction(){
        $data = $this->_getParam('data');
        $dataType = $this->_getParam('dataType');

        Zend_Loader::loadClass('DB_Users');
        $userModel = new DB_Users();

        $result = $userModel->isDataAlreadyUsed($data, $dataType);

        echo(Zend_Json::encode($result));
        die;
    }

    public function registerUserAction(){
        $data = $this->_getAllParams();
        unset($data['controller']);
        unset($data['action']);
        unset($data['module']);

        Zend_Loader::loadClass('DB_Users');
        $userModel = new DB_Users();

        $result['userId'] = $userModel->createUser($data);
        $result['success'] = (bool)$result['userId'];

        echo(Zend_Json::encode($result));
        die;
    }

    public function authAction(){
        $authData['login'] = $this->_getParam('login');
        $authData['password'] = $this->_getParam('password');

        Zend_Loader::loadClass('DB_Users');
        $userModel = new DB_Users();

        $result = (bool)$userModel->checkAuthData($authData);

        echo(Zend_Json::encode($result));
        die;
    }

    public function deAuthAction(){
        Zend_Loader::loadClass('DB_Users');
        $userModel = new DB_Users();

        $result = (bool)$userModel->destroySession();

        $this->_redirect('/');
    }

}

?>