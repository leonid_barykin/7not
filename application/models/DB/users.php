<?php

/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 03.03.2016
 * Time: 11:12
 */
class DB_Users extends Zend_Db_Table
{
    protected $_name = 'users';
    protected $_primary = array('id_user');

    public function testQuery()
    {
        $select = $this->select()->from($this->_name);
//        $select->setIntegrityCheck(false);
        $result = $this->fetchAll($select)->toArray();
        return $result;
    }

    public function createUser($data)
    {
        $dataToInsert = array();
        $dataToInsert['login'] = $data['login'];
        $dataToInsert['email'] = $data['email'];
        $dataToInsert['salt'] = $this->__generateSalt(3);
        $dataToInsert['password'] = md5(md5($data['password']) . md5($dataToInsert['salt']));
        $user_id = $this->insert($dataToInsert);

        return $user_id;
    }

    protected function __generateSalt($max = 15)
    {
        $characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $i = 0;
        $salt = "";
        do {
            $salt .= $characterList{mt_rand(0, strlen($characterList))};
            $i++;
        } while ($i <= $max);
        return $salt;
    }

    public function isDataAlreadyUsed($data, $dataType)
    {
        $select = $this->select()->from($this->_name);
        switch ($dataType) {
            case 'email':
                $select->where('email = ?', $data);
                break;
            case 'login':
                $select->where('login = ?', $data);
                break;
            default:
                break;
        }
        $result = $this->fetchAll($select)->toArray();

        return (bool)$result;
    }

    public function checkAuthData($authData)
    {

        $select = $this->select()->from($this->_name, array('password', 'salt'))->where('login = ?', $authData['login']);
        $realData = $this->fetchRow($select)->toArray();

        if ($realData['password'] == md5(md5($authData['password']) . md5($realData['salt']))) {
            $this->setSession();
            return true;
        } else {
            return false;
        }
    }

    private function setSession(){
        $auth = Zend_Auth::getInstance();
       $data['isUser'] = true;
        $auth->getStorage()->write($data);
    }

    public function destroySession(){
        Zend_Auth::getInstance()->clearIdentity();
    }

}