/**
 * Created by nikfu on 18.03.2016.
 */
$(function() {
    $(".contentdiv#materials").css("display","block");
    $(".btn").on("click", function(){
        var id = $(this).attr("id");
        if (id=="materials" || id=="friends" || id=="dialog" || id=="chosen" ) {
            $(".contentdiv").css("display", "none");
            $(".contentdiv#" + id).css("display", "block");
        }
    });

});