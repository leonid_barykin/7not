/**
 * Created by nikfu on 23.03.2016.
 */
$(document).ready(function() {
    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
    );
    $("#register").validate({
        rules: {
            Login: {
                required: true,
                regex:"^[A-Za-z-_]+$",
                maxlength: 50,

            },
            Email: {
                required: true,
                email: true,
            },
            Password: {
                required: true,
                regex:"(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}",
            },
            Password2: {
                required: true,
                equalTo: "#pass1",
            },
        },
        messages: {
            Login: {
                required: "Введите логин",
                regex:"Недопустимый формат",
                maxlength: "Недопустимая длина",

            },
            Email: {
                required: "Введите Email",
                email: "Введите Email",
            },
            Password: {
                required: "Введите пароль",
                regex:"Пароль должен содержать не менее 8 и не более 64 символов, хотя бы одину строчную, заглавную букву латинского алфавита и символ",
            },
            Password2: {
                required: "Введите пароль",
                equalTo: "Пароли не совпадают",
            },
        },
    });
});
