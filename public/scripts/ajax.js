/**
 * Created by Alexey on 03.03.2016.
 */
function getXMLHTTPRequest() {
    var req = false;
    try {
        /* для Firefox */
        req = new XMLHttpRequest();
    } catch (err) {
        try {
            /* для некоторых версий IE */
            req = new ActiveXObject("MsXML2.XMLHTTP");
        } catch (err) {
            try {
                /* для других версий IE */
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (err) {
                req = false;
            }
        }
    }
    return req;
}

function checkDataUsage(data, dataType) {
    var myReq = getXMLHTTPRequest();
    var page = 'auth/check-data-usage';
    var theURL = page + "?data=" + data + "&dataType=" + dataType;
    myReq.open("GET", theURL, true);
    myReq.overrideMimeType('text/xml');
    myReq.onreadystatechange = function () {
        if (myReq.readyState == 4) {
            if (myReq.status == 200) {
                var result = JSON.parse(myReq.responseText);
                if (result == true) {
                    switch (dataType) {
                        case 'email':
                            document.getElementsByName('usedEmail')[0].style.display = 'block';
                            break
                        case 'login':
                            document.getElementsByName('usedLogin')[0].style.display = 'block';
                            break
                        default:
                            alert('Неверное значение типа передаваемых данных')
                    }
                    document.getElementsByName('regButtom')[0].disabled = true;
                } else {
                    switch (dataType) {
                        case 'email':
                            document.getElementsByName('usedEmail')[0].style.display = 'none';
                            break
                        case 'login':
                            document.getElementsByName('usedLogin')[0].style.display = 'none';
                            break
                        default:
                            alert('Неверное значение типа передаваемых данных')
                    }
                    document.getElementsByName('regButtom')[0].disabled = false;
                }
            }
        }
    };
    myReq.send(null);
}

function registerUser() {
    var myReq = getXMLHTTPRequest();
    var page = 'auth/register-user';
    var theURL = page;
    var params = "login=" + document.getElementsByName('login')[0].value
        + "&email=" + document.getElementsByName('email')[0].value
        + "&password=" + document.getElementsByName('password')[0].value;
    myReq.open("POST", theURL, true);
    myReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    myReq.setRequestHeader("Content-length", params.length);
    myReq.setRequestHeader("Connection", "close");
    myReq.overrideMimeType('text/xml');
    myReq.onreadystatechange = function () {
        if (myReq.readyState == 4) {
            if (myReq.status == 200) {
                var result = JSON.parse(myReq.responseText);
                if (result.success == true) {
                    document.getElementsByName('inputForm')[0].innerHTML = 'Поздравляем, Вы успешно зарегистрированы! user_id = ' + result.userId;
                } else {
                    checkLoginUsage(document.getElementsByName('login')[0].value);
                }
                //else {
                //    if (result.fail == 'login') {
                //        checkLoginUsage(document.getElementsByName('login')[0].value);
                //    } else {
                //        document.getElementsByName('regError')[0].style.display = 'block';
                //        document.getElementsByName('pass')[0].value = '';
                //        document.getElementsByName('pass2')[0].value = '';
                //    }
                //}
            }
        }
    };
    myReq.send(params);
}

function authUser() {
    var myReq = getXMLHTTPRequest();
    var page = 'auth/auth';
    var theURL = page;
    var params = "login=" + document.getElementsByName('authLogin')[0].value + "&password=" + document.getElementsByName('authPass')[0].value;
    myReq.open("POST", theURL, true);
    myReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    myReq.setRequestHeader("Content-length", params.length);
    myReq.setRequestHeader("Connection", "close");
    myReq.overrideMimeType('text/xml');
    myReq.onreadystatechange = function () {
        if (myReq.readyState == 4) {
            if (myReq.status == 200) {
                var result = JSON.parse(myReq.responseText);
                if (result == true) {
                    document.location.reload(true)
                } else {
                    alert('Неправильный login или пароль');
//                    document.getElementsByName('authError')[0].style.display = 'block';
                }
            }
        }
    };
    myReq.send(params);
}

function deAuthUser() {
    var myReq = getXMLHTTPRequest();
    var page = 'auth/de-auth';
    var theURL = page;
    myReq.open("get", theURL, true);
    myReq.onreadystatechange = function () {
        if (myReq.readyState == 4) {
            if (myReq.status == 200) {
                document.location.reload(true);
            }
        }
    };
    myReq.send(null);
}